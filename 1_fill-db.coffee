config = require "./config"
request = require "request"
moment = require "moment"

require("./init-db")(true)
.then ->
    Competitor.destroy().complete (err) ->

      if err then throw err

      request "#{config.vkApiUrl}/users.search?fields=#{config.userFields.join(',')}&access_token=#{config.vkToken}&sex=1&city=#{config.donskoeId}&lang=ru&count=10000", (err, response, body) ->
        body = JSON.parse body 
        body.response.forEach (girl, index) ->
          unless index is 0
            if girl.bdate? and girl.bdate.split(".")[2]?
                girl.bdate = new Date(girl.bdate.split(".")[2] + "/" + girl.bdate.split(".")[1] + "/" + girl.bdate.split(".")[0])
            else
                girl.bdate = null
            Competitor.create girl
