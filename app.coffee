async = require "async"
fs = require "fs"
http = require "http"
https = require "https"
request = require "request"
Promise = require "bluebird"
require("./init-db")
_ = require "lodash"
moment = require "moment"


disableKeys = (fn, callback) ->
  async.series [
      (callback) ->
          sequelize.query("SET FOREIGN_KEY_CHECKS = 0").complete(callback)
      , fn
      , (callback) ->
          sequelize.query("SET FOREIGN_KEY_CHECKS = 1").complete(callback)

  ], callback

express = require("express")
path = require("path")
favicon = require("serve-favicon")
logger = require("morgan")
cookieParser = require("cookie-parser")
bodyParser = require("body-parser")
routes = require("./routes/index")
users = require("./routes/users")
app = express()
config = require("./config")
moment = require("moment")
app.locals.config = config

# view engine setup
app.set "views", path.join(__dirname, "views")
app.set "view engine", "jade"

# uncomment after placing your favicon in /public
#app.use(favicon(__dirname + '/public/favicon.ico'));
app.use bodyParser.json()
app.use bodyParser.urlencoded(extended: false)
app.use cookieParser()
app.use express.static(path.join(__dirname, "public"))
app.use logger("dev")
app.use "/", routes
app.use "/users", users

app.post "/delete/:uid", (req, res, next) ->
  sequelize.query "DELETE FROM Competitors WHERE uid = #{req.params.uid}"
  .then ->
    res.json {status: "OK"}
  .catch ->
    res.json {status: "FAIL"}

app.post "/podrocheno/:userId/:winId/:looseId", (req, res, next) ->
  # sequelize.query "SELECT count(UserId) as todayRounds FROM Results where UserId = #{req.params.userId} AND createdAt > '#{moment().format("YYYY-MM-DD")}'"
  sequelize.query "SELECT count(UserId) as todayRounds FROM Results where UserId = #{req.params.userId} AND TIMESTAMPDIFF(HOUR, createdAt, NOW()) < 1"
  .then (userTodayRounds) ->
    if +userTodayRounds[0].todayRounds < +config.roundsDailyMaxCount
      Result.create
        WinnerUid: req.params.winId
        LooserUid: req.params.looseId
        UserId: req.params.userId
      .then (user) ->
        res.json allowVote: true
    else res.json allowVote: false

app.post "/register", (req, res, next) ->
  User.find where: uid: req.body.uid
  .then (user) ->
    if user
      res.json({})
    else
      req.body.id = req.body.uid
      if req.body.bdate? and req.body.bdate.split(".")[2]?
        req.body.bdate = new Date(req.body.bdate.split(".")[2] + "/" + req.body.bdate.split(".")[1] + "/" + req.body.bdate.split(".")[0])
      else
        req.body.bdate = null
      User.create req.body
      .then (user) ->
        if user.bdate
          res.json user.toJSON()
        else
          # define bdate
          userFields = [
              "photo_max_orig"
              "screen_name"
              "relation"
              "sex"
              "bdate"
          ]

          request "#{config.vkApiUrl}/friends.get?fields=#{userFields.join(',')}&user_id=" + req.body.uid, (err, response, body) ->

            body = JSON.parse body

            friends = body.response

            # save friends
            Promise.each friends, (friend) ->
              Friend.find 
                where:
                  uid: friend.uid
                  CompetitorUid: req.body.uid
              .then (existingFriend) ->
                if existingFriend then return

                if friend.bdate? and friend.bdate.split(".")[2]?
                    friend.bdate = new Date(friend.bdate.split(".")[2] + "/" + friend.bdate.split(".")[1] + "/" + friend.bdate.split(".")[0])
                else 
                    friend.bdate = null

                friend.CompetitorUid = req.body.uid
                Friend.create friend
            .then ->

              # define bdate
              sequelize.query "select cust_date, count 
                  from (select *, count(id) as count,  YEAR(bdate) as cust_date from 
                    Friends  where CompetitorUid = #{req.body.uid} group by cust_date) as t1 where cust_date IS NOT NULL order by count DESC limit 1"
              .then (results) ->
                if results.length > 0

                  if +results[0].cust_date < 1985
                    results[0].cust_date = 1985
                  else if +results[0].cust_date > 2000
                    results[0].cust_date = 2000

                  unless results[0].cust_date
                    results[0].cust_date = 1994

                  user.updateAttributes 
                    bdate: new Date results[0].cust_date + "/01/02"
                  .then ->
                    res.json user.toJSON()
                else
                  res.json user.toJSON()
            .catch (err) ->
              res.json user.toJSON()
      .catch next

app.get "/girls-pair/:userId", (req, res, next) ->
  girl1 = girl2 = user = null
  firstSql = require "./get-first-competitor-sql"
  secondSql = require "./get-second-competitor-sql"

  User.find req.params.userId
  .then (_user) ->
    user = _user
    user.bdate = moment(user.bdate).format "YYYY-MM-DD"
    user.dataValues.bdate = moment(user.bdate).format "YYYY-MM-DD"
    firstSql =  _.template firstSql, user.dataValues
    console.log firstSql
    sequelize.query firstSql
  .then (competitors) ->
    Competitor.find competitors[0].uid
  .then (competitor) ->
    girl1 = competitor
    user.dataValues.excludeUid = girl1.uid
    secondSql =  _.template secondSql, user.dataValues
    console.log secondSql
    sequelize.query secondSql
  .then (competitors) ->
    Competitor.find competitors[0].uid
  .then (competitor) ->
    girl2 = competitor
    res.json [girl1.toJSON(), girl2.toJSON()]
  .catch (err) ->
    console.log err

# catch 404 and forward to error handler
app.use (req, res, next) ->
  err = new Error("Not Found")
  err.status = 404
  next err
  return


# error handlers

# development error handler
# will print stacktrace
if app.get("env") is "development"
  app.use (err, req, res, next) ->
    res.status err.status or 500
    res.render "error",
      message: err.message
      error: err

    return


# production error handler
# no stacktraces leaked to user
app.use (err, req, res, next) ->
  res.status err.status or 500
  res.render "error",
    message: err.message
    error: {}

  return


options = 
  key  : fs.readFileSync 'key.pem'
  cert : fs.readFileSync 'cert.pem'

http.createServer(app).listen 3000
https.createServer(options, app).listen 3001

module.exports = app