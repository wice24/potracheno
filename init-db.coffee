Sequelize = require "sequelize"
request = require "request"
global.config = require "./config"
global.sequelize = new Sequelize("potracheno", "wicer", "wicer",
  host: "127.0.0.1"
  logging: false
)

global.Competitor = sequelize.define("Competitor",
  uid: 
    type: Sequelize.STRING
    primaryKey: true
  photo_max_orig: Sequelize.STRING
  first_name: Sequelize.STRING
  last_name: Sequelize.STRING
  bdate: Sequelize.DATE
  customBdate: Sequelize.DATE
  failed: Sequelize.BOOLEAN
)

global.Friend = sequelize.define("Friend",
  uid: Sequelize.STRING
  photo_max_orig: Sequelize.STRING
  first_name: Sequelize.STRING
  last_name: Sequelize.STRING
  sex: Sequelize.STRING
  bdate: Sequelize.DATE
)

global.User = sequelize.define("User",
  uid: Sequelize.STRING
    type: Sequelize.STRING
    primaryKey: true
  first_name: Sequelize.STRING
  last_name: Sequelize.STRING
  bdate: Sequelize.DATE
)

global.Result = sequelize.define("Result", {})

Competitor.hasMany Friend, {foreignKey: "CompetitorUid"}

Friend.belongsTo Competitor, {onDelete: 'cascade', onUpdate: 'cascade'}

Result.belongsTo Competitor, {as: "Looser", foreignKey: "LooserUid"}
Result.belongsTo Competitor, {as: "Winner", foreignKey: "WinnerUid"}
Result.belongsTo User

module.exports = (force) ->
  sequelize.sync force: force
