var express = require('express');
var router = express.Router();
var Promise = require('bluebird');
var moment = require("moment");
var config = require("../config");
var _ = require("lodash");

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index');
});

router.get('/index', function(req, res) {
  res.render('index', {page: "index"});
});

router.post('/top/:userId', function(req, res) {
    var sql = "SELECT count(UserId) as todayRounds FROM Results where UserId = " + req.params.userId + " AND TIMESTAMPDIFF(HOUR, createdAt, NOW()) < 1"
    sequelize.query(sql)
    .then(function(userTodayRounds) {
        if (+userTodayRounds[0].todayRounds < +config.roundsDailyRequiredCount) {
            res.json({
                allowView: false,
                roundsToTop: config.roundsDailyRequiredCount - userTodayRounds[0].todayRounds
            });
        } else {
            res.json({
                allowView: true
            });
        }
    });
});

router.get('/admin', function(req, res) {
    Promise.all([
        sequelize.query("select * from Competitors")
    ]).spread(function(competitors) {
        _.map(competitors, function(c) {
            c.customBdate = moment(c.customBdate).format("YYYY");
            return c
        });

        res.render('admin', {
            page: "admin",
            competitors: competitors
        });
    });
});

router.get('/top/:userId', function(req, res) {
    Promise.all([
        sequelize.query("select count(id) as roundsAmount from Results"),
        sequelize.query("select *, count(id) AS winsCount from Competitors INNER JOIN Results ON (Results.WinnerUid = Competitors.uid) GROUP BY Competitors.uid order by winsCount DESC")
    ]).spread(function(results, competitors) {
        res.render('top', {
            page: "top",
            roundsAmount: results[0].roundsAmount,
            competitors: competitors
        });
    });
});

module.exports = router;
