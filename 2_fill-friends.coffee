config = require "./config"
request = require "request"
Promise = require "bluebird"

userFields = [
    "photo_max_orig"
    "screen_name"
    "relation"
    "sex"
    "bdate"
]

i = 0;
require("./init-db")()
.then ->
  saveFriends = (competitors, index, callback) ->
    return if competitors.length - 1 < index
    request "#{config.vkApiUrl}/friends.get?fields=#{userFields.join(',')}&user_id=" + competitors[index].uid, (err, response, body) ->
        console.log ++i
        if err
          competitors[index].updateAttributes
            failed: true
          return setTimeout (->
              saveFriends competitors, ++index
            ), 300

        try
          body = JSON.parse body
        
        catch err
          return setTimeout (->
              saveFriends competitors, ++index
            ), 300

        if body.error
          return setTimeout (->
              saveFriends competitors, ++index
            ), 300

        friends = body.response
        Promise.each friends, (friend) ->
          Friend.find 
            where:
              uid: friend.uid
              CompetitorUid: competitors[index].uid
          .then (existingFriend) ->
            if existingFriend then return

            if friend.bdate? and friend.bdate.split(".")[2]?
                friend.bdate = new Date(friend.bdate.split(".")[2] + "/" + friend.bdate.split(".")[1] + "/" + friend.bdate.split(".")[0])
            else 
                friend.bdate = null

            friend.CompetitorUid = competitors[index].uid
            Friend.create friend
        .then ->
          setTimeout (->
            saveFriends competitors, ++index
          ), 300

  Competitor.findAll
    where:
      customBdate: null
  .then (competitors) ->
    console.log competitors.length
    saveFriends competitors, 0
