var card1, card2, userId, ajaxInProgress = false;

$(function() {
    card1 = $('.card_left');
    card2 = $('.card_right');

    VK.callMethod("resizeWindow", 800, Math.max(640, $('body').height()));


    // $(".esko").click(function() {
    //     window.location = "https://www.youtube.com/watch?v=7p3EerDkyg0"
    // });

    $('.js-btn-skip').click(function() {
        getGirlsPair(function(err, res){
            updateCards(res);
        });
    });
    
    $('.js-btn-custom').click(function() {
        $elem = $(this);
        winId = $elem.attr("data-winId")
        looseId = $elem.attr("data-looseId")
        if (ajaxInProgress) {
            return
        }
        ajaxInProgress = true
        podrocheno(winId, looseId, function(res) {
            if (res.allowVote) {
                getGirlsPair(function(err, res){
                    updateCards(res);
                });
            } else {
                $("#vote-deny-modal").modal();
                ajaxInProgress = false
            }
        });
    });

    $(".top-link").click(function(e) {
        e.preventDefault();
        $.ajax({
            url: "/top/" + userId,
            type: "POST",
            success: function (res) {
                if (res.allowView) {
                    window.location.href = "/top/" + userId;
                } else {
                    $("#top-deny-modal").find(".rounds-to-top").html(res.roundsToTop);
                    $("#top-deny-modal").modal();
                }
            },
            error: function(err) {
                console.log(err);
            }
        });
    });

    $(".delete-nah").click(function(e) {
        var $elem = $(e.target);

        $.ajax({
            method: "POST",
            data: $elem.data("uid"),
            url: "/delete/" + $elem.data("uid"),
            success: function (res) {
                if (res.status === "OK") {
                    $("." + $elem.data("uid")).hide();
                }
                else {
                    aler("FAIL");
                }
            },
            error: function () {
                console.log("err")
            }
        })
    });

});


VK.init(function() { 
    VK.api("users.get", {test_mode:1, fields: "bdate"}, function(data) { 
        userId = data.response[0].uid;
        delete data.response[0].bdate
        $.ajax({
            url: "/register",
            type: "POST",
            data: data.response[0],
            success: function(res) {
                if (window.location.pathname === "/index") {
                    getGirlsPair(function(err, res) {
                        if(err) {
                            console.log(err);
                        } else {
                            updateCards(res);
                        }
                    });
                }
            },
            error: function(err) {
                console.log(err);
            }
        });
    });
}); 

function PreloadImage(imgSrc, callback){
  var objImagePreloader = new Image();

  objImagePreloader.src = imgSrc;
  if(objImagePreloader.complete){
    callback();
    objImagePreloader.onload=function(){};
  }
  else{
    objImagePreloader.onload = function() {
      callback();
      //    clear onLoad, IE behaves irratically with animated gifs otherwise
      objImagePreloader.onload=function(){};
    }
  }
}

function podrocheno (winId, looseId, callback) {
    $.ajax({
        url: "/podrocheno/" + userId + "/" + winId + "/" + looseId,
        type: "POST",
        success: function(res) {
            callback(res)
        },
        error: function(err) {
            callback(err);
        }
    });
}

function updateCards(res) {
    card1.addClass("card_in-progress");
    card2.addClass("card_in-progress");
    async.parallel([
        function (callback) { PreloadImage(res[0].photo_max_orig, callback)},
        function (callback) { PreloadImage(res[1].photo_max_orig, callback)},
        function (callback) { setTimeout(callback, 1000)}
    ], function () {
        ajaxInProgress = false
        card1.removeClass("card_in-progress");
        card2.removeClass("card_in-progress");

        card1.css("background-image", "url(" + res[0].photo_max_orig + ")");
        card1.siblings(".homosapiens-name").text(res[0].first_name + " " + res[0].last_name);
        card1.siblings(".js-btn-custom").attr("data-winId", res[0].uid);
        card1.siblings(".js-btn-custom").attr("data-looseId", res[1].uid);

        card2.css("background-image", "url(" + res[1].photo_max_orig + ")");
        card2.siblings(".homosapiens-name").text(res[1].first_name + " " + res[1].last_name);
        card2.siblings(".js-btn-custom").attr("data-winId", res[1].uid);
        card2.siblings(".js-btn-custom").attr("data-looseId", res[0].uid);

    });
}

function getGirlsPair (callback) {
    $.ajax({
        url: "/girls-pair/" + userId,
        type: "GET",
        success: function(res) {
            callback(null, res);
        },
        error: function(err) {
            callback(err);
        }
    });
}

