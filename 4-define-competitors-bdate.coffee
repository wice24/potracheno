config = require "./config"
request = require "request"
moment = require "moment"
Promise = require "bluebird"

i = 0;

require("./init-db")()
.then ->
  range = 100;
  Competitor.findAll
    where:
      customBdate: null
  .then (competitors) ->
    competitors.forEach (competitor) ->
      sql = "select cust_date, count 
          from (select *, count(id) as count,  YEAR(bdate) as cust_date from 
            friends  where CompetitorUid = #{competitor.uid} group by cust_date) as t1 where cust_date IS NOT NULL order by count DESC limit 1"
      sequelize.query sql
      .then (results) ->
        if results.length > 0
          console.log ++i
          competitor.updateAttributes 
            customBdate: new Date(results[0].cust_date + "/01/02")
          .catch ->
            console.log arguments
