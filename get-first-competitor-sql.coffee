# This query return all competitors with win, loose and sum amount of competitions and date of birthday

module.exports = "select * from (select *, ABS(COALESCE(DATEDIFF(customBdate,'<%= bdate %>'), 99999999)) as ageDiff FROM
    (SELECT t3.uid, t3.looseBattleAmount, t3.winBattleAmount, (t3.looseBattleAmount + t3.winBattleAmount) as sum, t3.customBdate FROM 

        (SELECT t.uid, t.winBattleAmount, COALESCE(t1.looseBattleAmount, 0) as looseBattleAmount, COALESCE(t.customBdate, t1.customBdate) as customBdate  FROM 
            
            (SELECT Competitors.uid, count(uid) as winBattleAmount, Competitors.customBdate FROM 
                Competitors INNER JOIN Results ON (Competitors.uid = Results.WinnerUid) WHERE Results.UserId = '<%= id %>' GROUP BY Results.WinnerUid) as t 

            LEFT JOIN 
                
            (SELECT Competitors.uid, count(`uid`) as looseBattleAmount, Competitors.customBdate FROM 
                Competitors INNER JOIN Results ON (Competitors.uid = Results.LooserUid) WHERE Results.UserId = '<%= id %>' GROUP BY Results.LooserUid) as t1

            ON (t.uid = t1.uid)

        UNION

        SELECT t2.uid, COALESCE(t1.winBattleAmount, 0) as winBattleAmount, t2.looseBattleAmount, COALESCE(t1.customBdate, t2.customBdate) as customBdate FROM 
        
            (SELECT Competitors.uid, count(uid) as winBattleAmount, Competitors.customBdate FROM  
                Competitors INNER JOIN Results ON (Competitors.uid = Results.WinnerUid) WHERE Results.UserId = '<%= id %>' GROUP BY Results.WinnerUid) as t1

            RIGHT JOIN

            (SELECT Competitors.uid, count(`uid`) as looseBattleAmount, Competitors.customBdate FROM
                Competitors INNER JOIN Results ON (Competitors.uid = Results.LooserUid) WHERE Results.UserId = '<%= id %>' GROUP BY Results.LooserUid) as t2

            ON (t1.uid = t2.uid)
        ) as t3
    ) as t4 WHERE sum < ((SELECT count(Competitors.uid) FROM Competitors))

UNION

select uid, 0, 0, 0, customBdate,  ABS(COALESCE(DATEDIFF(customBdate,'<%= bdate %>'), 99999999)) as ageDiff FROM Competitors 

order by rand()) as t5 where ageDiff < 1200"
